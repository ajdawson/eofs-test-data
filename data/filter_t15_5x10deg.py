"""Filter test data."""

#: Input file name.
INPUT_FILE = 'pacific_sst_test_data.txt'
#: Output file name.
OUTPUT_FILE = 'pacific_sst_test_data_t15_5x10deg.txt'

#: Latitudes to keep.
LATITUDES = [-25.5, -20.5, -15.5, -10.5, -5.5, -0.5,
             4.5, 9.5, 14.5, 19.5, 24.5]
#: Longitudes to keep.
LONGITUDES = [114.5, 124.5, 134.5, 144.5, 154.5, 164.5, 174.5, 184.5, 194.5,
              204.5, 214.5, 224.5, 234.5, 244.5, 254.5, 264.5, 274.5, 284.5]
#: Number of times to keep.
NTIMES = 15


def filter_lines(lines):
    """
    Filter out lines for the specified latitudes and longitudes and
    number of times.

    """
    out_lines = []
    for line in lines:
        lon, lat, time, value = line.split()
        if float(lat) in LATITUDES and float(lon) in LONGITUDES:
            out_lines.append(line)
    # check consistency
    map_size = len(LATITUDES) * len(LONGITUDES)
    if len(out_lines) % map_size:
        raise ValueError('inconsistent map size')
    out_lines = out_lines[:map_size * 15]
    return out_lines


with open(INPUT_FILE, 'r') as f:
    lines = filter_lines(f.readlines())

with open(OUTPUT_FILE, 'w') as f:
    f.write("".join(lines))
