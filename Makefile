#
# Build software to compute reference EOF solutions.
#

FC=gfortran
LDFLAGS=-llapack -lblas
DEOF_SRC=deof_src/deof.F deof_src/deofcovcor.F deof_src/deofpcs.F deof_src/deoftotvar.F
DEOF_OBJ=$(DEOF_SRC:.F=.o)
DEOF_FFLAGS=-c -cpp -ffixed-line-length-132 -O3

.PHONY: clean cleanoutput weighting.mod

deof_solution: src/eof_solution.o src/weighting.o deof_src/deoflib.a
	$(FC) -o deof_solution.exe src/eof_solution.o src/weighting.o deof_src/deoflib.a $(LDFLAGS)

src/eof_solution.o: src/eof_solution.f90 src/weighting.mod
	$(FC) -c -cpp -Wall -O3 $< -o $@

src/weighting.mod: src/weighting.o

src/weighting.o: src/weighting.f90
	$(FC) -c -cpp -O3 $< -o $@ -J src

deof_src/deoflib.a: $(DEOF_OBJ)
	$(AR) rvs $@ $(DEOF_OBJ)

deof_src/%.o: deof_src/%.F
	$(FC) $(DEOF_FFLAGS) -o $@ $<

clean:
	rm -f deof_src/*.o deof_src/*.a src/*.o src/*.mod *.exe

cleanoutput:
	rm -rf deof_solution post_processed
