program eof_solutions
!
! Purpose:
! ========
!   Uses the EOF software written by David Pierce (available at
!   http://meteora.ucsd.edu/~pierce/eof/eofs.html) to produce reference
!   solutions for testing the Python eofs library.
!
! History:
! ========
!   Author              Date          Notes
!   ------              ----          -----
!   Andrew Dawson       2012-12-23    Original version.
!
    use weighting
    implicit none

    ! Parameters for the input data shape.
    integer, parameter :: nt = 15                ! number of time-steps
    integer, parameter :: nlon = 18              ! number of longitudes
    integer, parameter :: nlat = 11              ! number of latitudes
    integer, parameter :: ns = nlat * nlon       ! number of grid points
    integer, parameter :: minst = min(ns, nt)
    integer, parameter :: maxst = max(ns, nt)
    integer, parameter :: nmodes = 14            ! number of EOF modes

    ! Input I/O unit.
    integer, parameter :: iunit = 10
    
    ! Output I/O units.
    integer, parameter :: eofunit = 11
    integer, parameter :: pcsunit = 12
    integer, parameter :: evlunit = 13    
    integer, parameter :: varunit = 14
    integer, parameter :: wgtunit = 15
    integer, parameter :: corunit = 16
    integer, parameter :: covunit = 17
    integer, parameter :: ernunit = 18
    integer, parameter :: ersunit = 19
    integer, parameter :: regunit = 20
    
    ! Output file names.
    character (len=64) :: eofname
    character (len=64) :: pcsname
    character (len=64) :: evlname
    character (len=64) :: varname
    character (len=64) :: wgtname
    character (len=64) :: corname
    character (len=64) :: covname
    character (len=64) :: regname
    character (len=64) :: ernname
    character (len=64) :: ersname

    ! Outputs of the deof subroutine.
    real(kind=8), dimension(ns, nt)     :: sst
    real(kind=8), dimension(nmodes)     :: eigenvalues
    real(kind=8), dimension(ns, nmodes) :: eigenvectors
    real(kind=8), dimension(ns, nmodes) :: eofscor
    real(kind=8), dimension(ns, nmodes) :: eofscov
    real(kind=8), dimension(ns, nmodes) :: eofsreg
    real(kind=8), dimension(nt, nmodes) :: pcs
    real(kind=8), dimension(nt, nmodes) :: spcs
    real(kind=8), dimension(nmodes)     :: variance
    real(kind=8), dimension(nmodes)     :: cumvariance

    ! Workspace and other input variables for the deof subroutine.
    real(kind=8), dimension(minst*(minst+1)/2) :: spacked
    real(kind=8), dimension(minst)             :: evals
    real(kind=8), dimension(minst, nmodes)     :: evecs
    real(kind=8), dimension(8*minst)           :: rlawork
    real(kind=8), dimension(maxst, nmodes)     :: tentpcs
    real(kind=8), dimension(ns)                :: weights
    integer                                    :: icovcor
    integer,      dimension(5*minst)           :: ilawork
    integer,      dimension(minst)             :: ifail

    ! Values read from file.
    real(kind=8)                  :: time
    real(kind=8)                  :: value
    real(kind=8), dimension(nlat) :: lat
    real(kind=8), dimension(nlon) :: lon
    
    ! Other processing variables.
    real(kind=8), dimension(ns)     :: mean
    real(kind=8), dimension(nmodes) :: norm
    real(kind=8), dimension(ns)     :: std
    real(kind=8)                    :: factor
    real(kind=8)                    :: sfactor
    real(kind=8), dimension(nmodes) :: terrors
    real(kind=8), dimension(nmodes) :: sterrors
    real(kind=8)                    :: pcstd

    ! Loop index and other counting variables.
    integer :: i, j, k, idx

    ! Command line argument handling.
    integer :: nargs
    character(len=64) :: method

    ! Get command line arguments, used to determine the weighting method used.
    nargs = command_argument_count()
    if (nargs /= 1) then
        method = 'equal'
    else
        call get_command_argument (1, method)
    end if
    
    ! Read the test data.
    write (*, '("reading test data")')
    open (unit=iunit, file='data/pacific_sst_test_data_t15_5x10deg.txt', &
          form='formatted', status='old')
    do k = 1, nt
        idx = 0
        do j = 1, nlat
            do i = 1, nlon
                read (iunit, *) lon(i), lat(j), time, value
                idx = idx + 1
                if (value .lt. -100.0d0 ) then
                    sst(idx, k) = 0.0d0
                else
                    sst(idx, k) = value
                end if
            end do
        end do
    end do
    close (iunit)

    ! Remove the mean along the time dimension.
    mean = sum(sst, 2) / float(nt)
    do k = 1, ns
        sst(k, :) = sst(k, :) - mean(k)
    end do

    ! Define grid weights.
    select case (trim(method))
        case ('equal')
            write (*, '("defining grid weights (equal)")')
            call equal_weights (nlat, nlon, lat, lon, weights)
        case ('latitude')
            write (*, '("defining grid weights (latitude)")')
            call latitude_weights (nlat, nlon, lat, lon, weights)
        case ('area')
            write (*, '("defining grid weights (area)")')
            call area_weights (nlat, nlon, lat, lon, weights)
        case ('area_multi')
            write (*, '("defining grid weights (area_multi)")')
            call area_weights (nlat, nlon, lat, lon, weights)
            weights = weights * sqrt(2.0d0)
        case ('area_multi_mix')
            write (*, '("defining grid weights (area_multi)")')
            call area_weights (nlat, nlon, lat, lon, weights)
            weights = weights * sqrt(2.0d0)
            k = 0
            do j = 1, nlat
                do i = 1, nlon
                    k = k + 1
                    if (i .le. (nlon / 2)) then
                        weights(k) = 1.0
                    end if
                end do
            end do
        case default
            write (*, '("unrecognised weighting scheme")')
            stop
    end select

    ! Call the deof subroutine to compute the required quantities.
    write (*, '("computing EOFs and related quantities")')
    icovcor = 0
    call deof (sst, ns, nt, nmodes, icovcor, &
               eigenvalues, eigenvectors, pcs, variance, cumvariance, &
               minst, maxst, spacked, evals, evecs, rlawork, ilawork, &
               ifail, tentpcs, weights)

    ! Scale the PCs to unit variance.
    norm = sqrt(sum(pcs * pcs, 1) / float(nt - 1))
    do k = 1, nmodes
        spcs(:, k) = pcs(:, k) / norm(k)
    end do

    ! Compute the correlation and covariance between the scaled PCs and the
    ! input SST at each gridpoint. First remove the weighting from the SST.
    do k = 1, nt
        do j = 1, ns
            if (weights(j) .eq. 0.0) then
                sst(j, k) = 0.0
            else
                sst(j, k) = sst(j, k) / weights(j)
            end if
        end do
    end do
    std = sqrt(sum(sst * sst, 2) / float(nt - 1))
    eofscov = matmul(sst, spcs) / float(nt - 1)
    do k = 1, ns
        eofscor(k, :) = eofscov(k, :) / std(k)
    end do
    do j = 1, nmodes
        pcstd = sqrt(sum(pcs(:, j) * pcs(:, j)) / float(nt - 1))
        do k = 1, ns
            eofsreg(k, j) = eofscor(k, j) * std(k) / pcstd
        end do
    end do
    

    ! Compute typical errors using North's rule of thumb and an equivalent
    ! form scaled to variance fraction.
    factor = sqrt(2.0d0 / float(nt))
    sfactor = factor / sum(eigenvalues)
    terrors = eigenvalues * factor
    sterrors = eigenvalues * sfactor

    ! Write the EOFs, PCs, eigenvalues and variances to file.
    write (*, '("writing solution to disk")')
    
    eofname = 'deof_solution/eofs.' // trim(method) // '.bin'
    open (unit=eofunit, file=eofname, form='unformatted', &
          access='direct', recl=8*nlat*nlon*nmodes)
    write (eofunit, rec=1) eigenvectors
    close (eofunit)

    pcsname = 'deof_solution/pcs.' // trim(method) // '.bin'
    open (unit=pcsunit, file=pcsname, form='unformatted', &
          access='direct', recl=8*nt*nmodes)
    write (pcsunit, rec=1) pcs
    close (pcsunit)

    evlname = 'deof_solution/eigenvalues.' // trim(method) // '.bin'
    open (unit=evlunit, file=evlname, form='unformatted', &
          access='direct', recl=8*nmodes)
    write (evlunit, rec=1) eigenvalues
    close (evlunit)

    varname = 'deof_solution/variance.' // trim(method) // '.bin'
    open (unit=varunit, file=varname, form='unformatted', &
          access='direct', recl=8*nmodes)
    write (varunit, rec=1) variance
    close (varunit)
    
    if (trim(method) /= 'equal') then
        wgtname = 'deof_solution/weights.' // trim(method) // '.bin'
        open (unit=wgtunit, file=wgtname, form='unformatted', &
              access='direct', recl=8*nlat*nlon)
        write (wgtunit, rec=1) weights
        close (wgtunit)
    end if

    corname = 'deof_solution/eofscor.' // trim(method) // '.bin'
    open (unit=corunit, file=corname, form='unformatted', &
          access='direct', recl=8*nlat*nlon*nmodes)
    write (corunit, rec=1) eofscor
    close (corunit)

    covname = 'deof_solution/eofscov.' // trim(method) // '.bin'
    open (unit=covunit, file=covname, form='unformatted', &
          access='direct', recl=8*nlat*nlon*nmodes)
    write (covunit, rec=1) eofscov
    close (covunit)

    regname = 'deof_solution/eofsreg.' // trim(method) // '.bin'
    open (unit=regunit, file=regname, form='unformatted', &
          access='direct', recl=8*nlat*nlon*nmodes)
    write (regunit, rec=1) eofsreg
    close (regunit)

    ernname = 'deof_solution/errors.' // trim(method) // '.bin'
    open (unit=ernunit, file=ernname, form='unformatted', &
          access='direct', recl=8*nmodes)
    write (ernunit, rec=1) terrors
    close (ernunit)

    ersname = 'deof_solution/scaled_errors.' // trim(method) // '.bin'
    open (unit=ersunit, file=ersname, form='unformatted', &
          access='direct', recl=8*nmodes)
    write (ersunit, rec=1) sterrors
    close (ersunit)

end program
