module weighting
!
! Purpose:
! ========
!
! Provides weights generation routines for EOF test programs.
!

    contains

    subroutine equal_weights (nlat, nlon, lat, lon, weights)
        implicit none
        integer, intent(in) :: nlat, nlon
        real(kind=8), dimension(nlat),      intent(in)  :: lat
        real(kind=8), dimension(nlon),      intent(in)  :: lon
        real(kind=8), dimension(nlat*nlon), intent(out) :: weights
        weights = 1
        return
    end subroutine equal_weights
    
    subroutine latitude_weights (nlat, nlon, lat, lon, weights)
        implicit none
        integer, intent(in) :: nlat, nlon
        real(kind=8), dimension(nlat),      intent(in)  :: lat
        real(kind=8), dimension(nlon),      intent(in)  :: lon
        real(kind=8), dimension(nlat*nlon), intent(out) :: weights
        integer :: i, j, k
        real(kind=8) :: d2r
        d2r = 4.0d0 * atan(1.0d0) / 180.0d0
        k = 0
        do j = 1, nlat
            do i = 1, nlon
                k = k + 1
                weights(k) = sqrt(cos(d2r * lat(j)))
            end do
        end do
        return
    end subroutine latitude_weights
    
    subroutine area_weights (nlat, nlon, lat, lon, weights)
        implicit none
        integer, intent(in) :: nlat, nlon
        real(kind=8), dimension(nlat),      intent(in)  :: lat
        real(kind=8), dimension(nlon),      intent(in)  :: lon
        real(kind=8), dimension(nlat*nlon), intent(out) :: weights
        integer :: i, j, k
        real(kind=8) :: d2r, lon0, lon1, lat0, lat1, dlon, dlat, R
        R = 6367470.0d0
        d2r = 4.0d0 * atan(1.0d0) / 180.0d0
        dlon = lon(2) - lon(1)
        dlat = lat(2) - lat(1)
        k = 0
        do j = 1, nlat
            lat0 = lat(j) - dlat / 2.0d0
            lat1 = lat(j) + dlat / 2.0d0
            do i = 1, nlon
                k = k + 1
                lon0 = lon(i) - dlon / 2.0d0
                lon1 = lon(i) + dlon / 2.0d0
                weights(k) = R * R * d2r * (lon1 - lon0) * &
                             (sin(d2r * lat1) - sin(d2r * lat0))
            end do
        end do
        weights = sqrt(weights / sum(weights))
    end subroutine area_weights
    
end module weighting
