Generation of test data for EOF analysis
========================================

Generate EOF solution for an input SST field using three different weighting schemes.
The output is used as a reference solution to test other EOF implementations.


Generating the test data
------------------------

Run the data generation script:

    python make-data.py

This script builds and runs the Fortran code that computes the solution and post-processes
the outputs which are written to the `post_processed` directory as NumPy binary files.
The directory `deof_solution` contains the flat binary output of the Fortran program.


Output data description
-----------------------

The output data is in NumPy binary format. Each output file name has the form:

    <name>.<weights type>.npy

where *name* is the name of the variable and *weights type* is the name of the weighting
scheme used in the computation. The three weight schemes are equal, for equal weighting; 
latitude, for square-root of cosine of latitude weighting; area, for square-root of normalized
grid-cell-area weighting. The input data and coordinate dimensions are also stored in this
directory in the same format. The number of EOF modes retained is 28, which includes all the
modes with non-zero eigenvalues. The following describes the output data files:

* `sst.npy` [time, latitude, longitude]: input data field

* `time.npy` [time]: time coordinate dimension, units 'months since 0-1-1 00:00:0.0'

* `latitude.npy` [latitude]: latitude coordinate dimension

* `longitude.npy` [longitude]: longitude coordinate dimension

* `eigenvalues.<weights_type>.npy` [eofs]: eigenvalues

* `eofs.<weights type>.npy` [eofs, latitude, longitude]: EOFs scaled to unit length

* `eofscov.<weights type>.npy` [eofs, latitude, longitude]: EOFs as covariance between PCs and input data field

* `eofscor.<weights type>.npy` [eofs, latitude, longitude]: EOFs as correlation between PCs and input data field

* `errors.<weights type>.npy` [eofs]: typical eigenvalue errors

* `scaled_errors.<weights type>.npy` [eofs]: typical errors scaled to variance fraction

* `pcs.<weights type>.npy` [time, eofs]: PCs scaled to unit variance

* `variance.<weights type>.npy` [eofs]: percentage variance explained by each mode

* `weights.<weights type>.npy` [latitude, longitude]: grid weights
