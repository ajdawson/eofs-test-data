"""
Post-process the EOF solution obtained using David Pierce's EOF software.

"""
# TODO: output
import glob
import os
import subprocess

import numpy as np
import numpy.ma as ma


# globals defining script behavior:
input_data_file = 'data/pacific_sst_test_data_t15_5x10deg.txt'
bin_data_dir = 'deof_solution'
post_processed_dir = 'post_processed'
ntime = 15
nlat = 11
nlon = 18
nmodes = 14


class Error(Exception):
    pass


def run_make():
    try:
        subprocess.check_call('make')
    except subprocess.CalledProcessError:
        raise Error('failed to build the data generation program')


def check_deof_dir():
    if not os.path.exists(bin_data_dir):
        os.mkdir(bin_data_dir)


def run_deof_solution(weighting):
    """
    Run the data generation program which uses David Pierce's EOF
    software to compute EOFs of a tropical SST field. The type of
    weighting to be used is an input to the program and may be one
    of 'equal', 'latitude' or 'area'.

    """
    try:
        subprocess.check_call(['./deof_solution.exe', weighting])
    except subprocess.CalledProcessError:
        raise Error('failed to run the data generation program')


def reconstructions():
    """
    Generate data for testing reconstructions with arbitrary modes.

    """
    for filename in glob.glob('{}/eofs.*.npy'.format(post_processed_dir)):
        name = filename.split('.')[1]
        eof_file = os.path.join(post_processed_dir, 'eofs.{}.npy'.format(name))
        pc_file = os.path.join(post_processed_dir, 'pcs.{}.npy'.format(name))
        eofs = np.load(eof_file)
        pcs = np.load(pc_file)
        ntime = pcs.shape[0]
        neofs = eofs.shape[0]
        space = eofs.shape[1:]
        nspace = np.product(space)
        pcs = pcs[:, [0, 1, 4]]
        eofs = eofs[[0, 1, 4]]
        rcon = np.dot(pcs, eofs.reshape([3, nspace])).reshape((ntime,) + space)
        # remove weighting
        if name != 'equal':
            weights = np.load(os.path.join(post_processed_dir, 'weights.{}.npy'.format(name)))
            rcon = rcon / weights
        outfile = 'rcon.{}'.format(name)
        np.save(os.path.join(post_processed_dir, outfile), rcon)


def post_process():
    """
    Post-process the output binary files into NumPy array binary files
    that can be read directly with NumPy. Also do the same for the input
    to the data generation program.
 
    """
    print 'running post-processing'
    data_shapes = {'eofs': [nmodes, nlat, nlon],
                   'eofscor': [nmodes, nlat, nlon],
                   'eofscov': [nmodes, nlat, nlon],
                   'eofsreg': [nmodes, nlat, nlon],
                   'pcs': [nmodes, ntime],
                   'eigenvalues': [nmodes],
                   'variance': [nmodes],
                   'weights': [nlat, nlon],
                   'errors': [nmodes],
                   'scaled_errors': [nmodes]}
    # Process the input to the data generation program.
    lon, lat, time, sst = np.loadtxt(input_data_file).transpose()
    lon = lon[:nlon]
    lat = lat[:nlat*nlon:nlon]
    time = time[::nlat*nlon]
    sst = sst.reshape([ntime, nlat, nlon])
    sst = ma.MaskedArray(sst, mask=sst<-100.).filled(fill_value=np.nan)
    if not os.path.exists(post_processed_dir):
        os.mkdir(post_processed_dir)
    np.save(os.path.join(post_processed_dir, 'time'), time)
    np.save(os.path.join(post_processed_dir, 'latitude'), lat)
    np.save(os.path.join(post_processed_dir, 'longitude'), lon)
    np.save(os.path.join(post_processed_dir, 'sst'), sst)
    # Process all outputs of the data generation program.
    for filename in os.listdir(bin_data_dir):
        data_type = filename.split('.')[0]
        fd = open(os.path.join(bin_data_dir, filename), 'rb')
        raw_data = np.fromfile(fd, dtype=np.float64)
        fd.close()
        data = raw_data.reshape(data_shapes[data_type])
        if data_type == 'pcs':
            data = data.transpose()
        if data_type == 'eofs':
            data[np.where(np.isnan(sst[:nmodes]))] = np.nan
        outfile = os.path.splitext(filename)[0]
        np.save(os.path.join(post_processed_dir, outfile), data)


def main():
    """Generate the test data."""
    run_make()
    check_deof_dir()
    run_deof_solution('equal')
    run_deof_solution('latitude')
    run_deof_solution('area')
    run_deof_solution('area_multi')
    run_deof_solution('area_multi_mix')
    post_process()
    reconstructions()

 
if __name__ == '__main__':
    main()
