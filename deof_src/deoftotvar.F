
	subroutine deoftotvar( data, nx, nt, totvar, doswitched,
     &		icovcor, covariance, correlation )

c	-------------------------------------------------
c	Returns the total variance in the field so we can
c	normalize by it.
c
c	Inputs:
c
c		data(nx,nt): data to calculate upon
c
c		nx, nt:	size of 'data'.
c
c		doswitched: if .TRUE., then we are working
c			in switched (nx,nt) space, false 
c			otherwise.
c
c		icovcor: if (icovcor .eq. covariance), then
c			we are using the covariance array;
c			if( icovcor .eq. correlation) then
c			we are using the correlation array.
c
c	Outputs:
c
c		totvar: estimate of total variance
c
c	-------------------------------------------------

	implicit none

	integer	nx, nt, icovcor, covariance, correlation
	double precision	data(nx,nt), totvar, sum, fact
	logical	doswitched
	integer	i, j

	if( icovcor .eq. correlation ) then
		if( doswitched ) then
			totvar = float( nt )
		else
			totvar = float( nx )
		endif
		return
	endif

	totvar = 0.0
	if( doswitched ) then
		fact = 1.0d0/float(nt-1)
	else
		fact = 1.0d0/float(nt-1)
	endif
	do j=1, nt
		sum = 0.0
		do i=1, nx
			sum = sum + data(i,j)*data(i,j)
		enddo
		totvar = totvar + sum*fact
	enddo

	return
	end

